# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Night Theme Switcher package.
# poi <erbaotao@outlook.com>, 2022, 2024.
# Dingzhong Chen <wsxy162@gmail.com>, 2022.
# Romain Vigier <romain@romainvigier.fr>, 2022, 2024.
# 0xarch <alanqa-ops@outlook.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: Night Theme Switcher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-24 11:18+0100\n"
"PO-Revision-Date: 2024-05-20 09:29+0000\n"
"Last-Translator: Romain Vigier <romain@romainvigier.fr>\n"
"Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/"
"night-theme-switcher/extension/zh_Hans/>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.6-dev\n"

#: src/data/ui/BackgroundButton.ui:8
msgid "Change background"
msgstr "更换背景"

#: src/data/ui/BackgroundButton.ui:24
msgid "Select your background image"
msgstr "选择您要用的背景图片"

#: src/data/ui/BackgroundsPage.ui:9
msgid "Backgrounds"
msgstr "背景"

#: src/data/ui/BackgroundsPage.ui:13
msgid ""
"Background switching is handled by the Shell, these settings allow you to "
"change the images it uses."
msgstr "背景切换由 Shell 控制，这里选择的图像将由 Shell 进行切换。"

#: src/data/ui/BackgroundsPage.ui:16
msgid "Day background"
msgstr "日间背景"

#: src/data/ui/BackgroundsPage.ui:29
msgid "Night background"
msgstr "夜间背景"

#: src/data/ui/CommandsPage.ui:9
msgid "Commands"
msgstr "命令"

#: src/data/ui/CommandsPage.ui:13
msgid "Run commands"
msgstr "运行命令"

#: src/data/ui/CommandsPage.ui:21 src/data/ui/SchedulePage.ui:21
msgid "Sunrise"
msgstr "日出"

#: src/data/ui/CommandsPage.ui:28 src/data/ui/SchedulePage.ui:34
msgid "Sunset"
msgstr "日落"

#: src/data/ui/ContributePage.ui:9
msgid "Contribute"
msgstr "支持本软件"

#: src/data/ui/ContributePage.ui:42
msgid "View the code and report issues"
msgstr "查看源代码，报告软件错误"

#: src/data/ui/ContributePage.ui:55
msgid "Contribute to the translation"
msgstr "帮忙翻译本软件"

#: src/data/ui/SchedulePage.ui:9
msgid "Schedule"
msgstr "安排"

#: src/data/ui/SchedulePage.ui:15
msgid "Manual schedule"
msgstr "手动安排"

#: src/data/ui/SchedulePage.ui:16
msgid "Use a manual schedule instead of the one computed from your location."
msgstr "使用手动设置的时间代替从位置计算的时间。"

#: src/data/ui/SchedulePage.ui:51
msgid "Run the transition when an app is fullscreen"
msgstr "当有程序全屏时也进行过渡"

#: src/data/ui/SchedulePage.ui:60
msgid "Keyboard shortcut"
msgstr "键盘快捷键"

#: src/data/ui/ShortcutButton.ui:15
msgid "Choose…"
msgstr "选择……"

#: src/data/ui/ShortcutButton.ui:32
msgid "Change keyboard shortcut"
msgstr "更改键盘快捷键"

#: src/data/ui/ShortcutButton.ui:43
msgid "Clear"
msgstr "清空"

#: src/data/ui/ShortcutButton.ui:78
msgid "Press your keyboard shortcut…"
msgstr "按下您想设置的键盘快捷键……"

#. Time separator (eg. 08:27)
#: src/data/ui/TimeChooser.ui:33 src/data/ui/TimeChooser.ui:91
msgid ":"
msgstr ":"

#: src/data/ui/TimeChooser.ui:52
msgid "AM"
msgstr "上午"

#: src/data/ui/TimeChooser.ui:58
msgid "PM"
msgstr "下午"

#: src/modules/Timer.js:285
msgid "Unknown Location"
msgstr "地理位置未知"

#: src/modules/Timer.js:286
msgid "A manual schedule will be used to switch the dark mode."
msgstr "会依照手动安排切换深色模式。"

#: src/modules/Timer.js:290
msgid "Edit Manual Schedule"
msgstr "手动进行安排"

#: src/preferences/BackgroundButton.js:94
msgid "This image format is not supported."
msgstr "这种图像格式不能使用。"

#~ msgid "Open Location Settings"
#~ msgstr "打开地理位置设置"

#~ msgid "notify-send \"Hello sunshine!\""
#~ msgstr "notify-send \"漱正阳而含朝霞\""

#~ msgid "notify-send \"Hello moonshine!\""
#~ msgstr "notify-send \"有月影 在水面 漂流不定\""

#~ msgid "Support me with a donation"
#~ msgstr "捐款支持我开发本软件"

#~ msgid ""
#~ "Your location will be used to calculate the current sunrise and sunset "
#~ "times. If you prefer, you can set a manual schedule. It will also be used "
#~ "when your location is unavailable."
#~ msgstr ""
#~ "每天日出日落的时间会根据您所在的地理位置进行推算，您也可以转而选择手动安排"
#~ "时间。地理位置未知时，也会按照手动安排的进行。"

#~ msgid "Themes"
#~ msgstr "主题"

#~ msgid ""
#~ "GNOME has a built-in dark mode that the extension uses. Manually "
#~ "switching themes is discouraged and is only here for legacy reasons."
#~ msgstr ""
#~ "GNOME 有自带的、兼容本扩展程序的深色模式。最好只在旧版不兼容时再手动切换主"
#~ "题，其他情况下最好不要手动切换。"

#~ msgid "Switch GTK theme"
#~ msgstr "切换 GTK 的主题"

#~ msgid ""
#~ "Forcing a dark theme on applications not designed to support it can have "
#~ "undesirable side-effects such as unreadable text or invisible icons."
#~ msgstr ""
#~ "对于设计时没有考虑深色主题的软件，非要使用深色主题可能会有“不良反应”，比如"
#~ "说，文字可能没法读、图标可能看不见。"

#~ msgid "Day variant"
#~ msgstr "日间样式"

#~ msgid "Night variant"
#~ msgstr "夜间模式"

#~ msgid "Switch Shell theme"
#~ msgstr "切换 Shell 的主题"

#~ msgid "Switch icon theme"
#~ msgstr "切换图标的主题"

#~ msgid "Switch cursor theme"
#~ msgstr "切换指针主题"

#, javascript-format
#~ msgid "Version %d"
#~ msgstr "%d 版"

#~ msgid "Default"
#~ msgstr "默认"

#~ msgid ""
#~ "Only JPEG, PNG, TIFF, SVG and XML files can be set as background image."
#~ msgstr "背景图片必须是 JPEG、PNG、TIFF、SVG 或 XML 格式。"

#~ msgid "Settings version"
#~ msgstr "设置的版本"

#~ msgid "The current extension settings version"
#~ msgstr "当前本扩展程序设置的版本"

#~ msgid "Enable GTK variants switching"
#~ msgstr "开始 GTK 样式的切换"

#~ msgid "Day GTK theme"
#~ msgstr "日间 GTK 主题"

#~ msgid "The GTK theme to use during daytime"
#~ msgstr "日间要使用的 GTK 主题"

#~ msgid "Night GTK theme"
#~ msgstr "夜间 GTK 主题"

#~ msgid "The GTK theme to use during nighttime"
#~ msgstr "夜间要使用的 GTK 主题"

#~ msgid "Enable shell variants switching"
#~ msgstr "开始 shell 样式的切换"

#~ msgid "Day shell theme"
#~ msgstr "日间 shell 主题"

#~ msgid "The shell theme to use during daytime"
#~ msgstr "日间要使用的 shell 主题"

#~ msgid "Night shell theme"
#~ msgstr "夜间 shell 主题"

#~ msgid "The shell theme to use during nighttime"
#~ msgstr "夜间要使用的 shell 主题"

#~ msgid "Enable icon variants switching"
#~ msgstr "开始图标样式的切换"

#~ msgid "Day icon theme"
#~ msgstr "日间图标主题"

#~ msgid "The icon theme to use during daytime"
#~ msgstr "白天要使用的图标的主题"

#~ msgid "Night icon theme"
#~ msgstr "夜间图标主题"

#~ msgid "The icon theme to use during nighttime"
#~ msgstr "晚上要使用的图标的主题"

#~ msgid "Enable cursor variants switching"
#~ msgstr "开始指针样式的切换"

#~ msgid "Day cursor theme"
#~ msgstr "日间指针主题"

#~ msgid "The cursor theme to use during daytime"
#~ msgstr "要白天使用的指针的主题"

#~ msgid "Night cursor theme"
#~ msgstr "夜间指针主题"

#~ msgid "The cursor theme to use during nighttime"
#~ msgstr "晚上要使用的指针的主题"

#~ msgid "Enable commands"
#~ msgstr "使用命令功能"

#~ msgid "Commands will be spawned on time change"
#~ msgstr "随时间变化生成命令"

#~ msgid "Sunrise command"
#~ msgstr "日出命令"

#~ msgid "The command to spawn at sunrise"
#~ msgstr "日出时生成此命令"

#~ msgid "Sunset command"
#~ msgstr "日落命令"

#~ msgid "The command to spawn at sunset"
#~ msgstr "日落时生成此命令"

#~ msgid "Path to the day background"
#~ msgstr "白天用的背景的路径"

#~ msgid "Path to the night background"
#~ msgstr "晚上用的背景的路径"

#~ msgid "Time source"
#~ msgstr "时间获取来源"

#~ msgid "The source used to check current time"
#~ msgstr "确定现在什么时候了的根据"

#~ msgid "Follow Night Light \"Disable until tomorrow\""
#~ msgstr "跟随夜灯的“明天前禁用”状态"

#~ msgid "Switch back to day time when Night Light is temporarily disabled"
#~ msgstr "如果夜灯暂时关上，就换回日间模式"

#~ msgid "Always enable the on-demand timer"
#~ msgstr "不关闭自定的定时"

#~ msgid "The on-demand timer will always be enabled alongside other timers"
#~ msgstr "自定的定时系统会和其他定时系统一起生效"

#~ msgid "On-demand time"
#~ msgstr "自定时间"

#~ msgid "The current time used in on-demand mode"
#~ msgstr "自定模式依据的当前时间"

#~ msgid "Key combination to toggle time"
#~ msgstr "切换时间的按键组合"

#~ msgid "The key combination that will toggle time in on-demand mode"
#~ msgstr "此按键组合会切换自定模式依据的时间"

#~ msgid "On-demand button placement"
#~ msgstr "自定按钮的位置"

#~ msgid "Where the on-demand button will be placed"
#~ msgstr "自定按钮放在哪里"

#~ msgid "Use manual time source"
#~ msgstr "手动设置时间"

#~ msgid "Disable automatic time source detection"
#~ msgstr "不自动设置时间"

#~ msgid "Sunrise time"
#~ msgstr "日出时间"

#~ msgid "When the day starts"
#~ msgstr "白天什么时候开始"

#~ msgid "Sunset time"
#~ msgstr "日落时间"

#~ msgid "When the day ends"
#~ msgstr "白天什么时候结束"

#~ msgid "Transition"
#~ msgstr "渐变过渡"

#~ msgid "Use a transition when changing the color scheme"
#~ msgstr "渐变地切换色彩设计"

#~ msgid "Manual time source"
#~ msgstr "手动设置时间来源"

#~ msgid ""
#~ "The extension will try to use Night Light or Location Services to "
#~ "automatically set your current sunrise and sunset times if they are "
#~ "enabled. If you prefer, you can manually choose a time source."
#~ msgstr ""
#~ "如果开启了夜灯功能或定位服务，本扩展程序会尝试据此自动设置当前日出与日落时"
#~ "间。您也可以手动选定一个确定时间的根据。"

#~ msgid "Always show on-demand controls"
#~ msgstr "一直显示“自定”的控制选项"

#~ msgid "Allows you to override the current time when using a schedule."
#~ msgstr "用于在安排时强制设定当前时间。"

#~ msgid "Advanced"
#~ msgstr "高级设置"

#~ msgid "Smooth transition between day and night appearance."
#~ msgstr "渐变地过渡日间模式和夜间模式。"

#~ msgid "Night Light"
#~ msgstr "夜灯"

#~ msgid "These settings only apply when Night Light is the time source."
#~ msgstr "这些设置仅在夜灯作为时间源时应用。"

#~ msgid "Follow <i>Disable until tomorrow</i>"
#~ msgstr "跟随<i>明天前禁用</i>"

#~ msgid ""
#~ "When Night Light is temporarily disabled, the extension will switch to "
#~ "day variants."
#~ msgstr "如果夜灯暂时关上，就换到日间模式。"

#~ msgid ""
#~ "These settings only apply when using the manual schedule as the time "
#~ "source."
#~ msgstr "这些设置仅在使用手动安排作为时间源时应用。"

#~ msgid "On-demand"
#~ msgstr "自定"

#~ msgid "These settings only apply when using the on-demand time source."
#~ msgstr "这些设置仅在使用自定时间源时应用。"

#~ msgid "Turn Night Mode Off"
#~ msgstr "关闭夜间模式"

#~ msgid "Turn Night Mode On"
#~ msgstr "开启夜间模式"

#~ msgid "Night Mode Off"
#~ msgstr "夜间模式已关闭"

#~ msgid "Night Mode On"
#~ msgstr "夜间模式已开启"

#~ msgid "Turn Off"
#~ msgstr "关闭"

#~ msgid "Turn On"
#~ msgstr "开启"

#~ msgid "Location Services"
#~ msgstr "使用定位服务"

#~ msgid "None"
#~ msgstr "不要按钮"

#~ msgid "Top bar"
#~ msgstr "放在上边栏里"

#~ msgid "System menu"
#~ msgstr "放在系统菜单里"

#~ msgid "Switch GTK variants"
#~ msgstr "切换 GTK 样式"

#~ msgid "Switch shell variants"
#~ msgstr "切换 shell 样式"

#~ msgid "Switch icon variants"
#~ msgstr "切换图标样式"

#~ msgid "Switch cursor variants"
#~ msgstr "切换指针样式"

#~ msgid "GTK theme"
#~ msgstr "GTK 主题"

#~ msgid "Shell theme"
#~ msgstr "Shell 主题"

#~ msgid "Icon theme"
#~ msgstr "图标主题"

#~ msgid "Cursor theme"
#~ msgstr "指针主题"

#~ msgid "Manual schedule times"
#~ msgstr "手动安排时间"

#~ msgid "Enable backgrounds"
#~ msgstr "背景也要切换"

#~ msgid "Background will be changed on time change"
#~ msgstr "时间变化，背景跟着切换"

#, javascript-format
#~ msgid ""
#~ "Unable to automatically detect the day and night variants for the \"%s\" "
#~ "GTK theme. Please manually choose them in the extension's preferences."
#~ msgstr ""
#~ "无法自动监测“%s”GTK 的主题的日间样式和夜间样式。请在本扩展程序的个人设置中"
#~ "手动选择。"

#, javascript-format
#~ msgid ""
#~ "Unable to automatically detect the day and night variants for the \"%s\" "
#~ "GNOME Shell theme. Please manually choose them in the extension's "
#~ "preferences."
#~ msgstr ""
#~ "无法自动监测“%s”GNOME Shell 的主题的日间样式和夜间样式。请在本扩展程序的个"
#~ "人设置中手动选择。"

#~ msgid "Switch to night theme"
#~ msgstr "切换到夜间主题"

#~ msgid "Switch to day theme"
#~ msgstr "切换到日间主题"

#~ msgid "Use manual GTK variants"
#~ msgstr "手动设置 GTK 样式"

#~ msgid "Disable automatic GTK theme variants detection"
#~ msgstr "不自动监测 GTK 的主题样式"

#~ msgid "Use manual shell variants"
#~ msgstr "手动设置 shell 样式"

#~ msgid "Disable automatic shell theme variants detection"
#~ msgstr "不自动监测 shell 的主题样式"

#~ msgid "Switch backgrounds"
#~ msgstr "切换背景"

#~ msgid "Switch cursor theme variants"
#~ msgstr "切换指针的主题样式"

#~ msgid "Manual variants"
#~ msgstr "手动设置样式"

#~ msgid ""
#~ "You can manually set variants if the extension cannot automatically "
#~ "detect the day and night variants of your GTK theme. Please <a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">submit a request</a> to get your theme supported."
#~ msgstr ""
#~ "如果本扩展程序没能自动检测到您 GTK 的主题的日间样式和夜间样式的话，您可以"
#~ "手动设置样式。想让扩展能成功识别您的主题？您可以<a href=\"https://gitlab."
#~ "com/rmnvgr/nightthemeswitcher-gnome-shell-extension/-/issues\">提交请求</"
#~ "a>。"

#~ msgid "Support us"
#~ msgstr "帮助我们"

#~ msgid "View code on GitLab"
#~ msgstr "在 GitLab 上查看本扩展源代码"

#~ msgid "Translate on Weblate"
#~ msgstr "在 Weblate 上翻译本扩展"

#~ msgid "Donate on Liberapay"
#~ msgstr "用 Liberapay 向我们捐款"

#~ msgid "Appearance"
#~ msgstr "外观"

#~ msgid ""
#~ "You can manually set variants if the extension cannot automatically "
#~ "detect the day and night variants of your GNOME Shell theme. Please <a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">submit a request</a> to get your theme supported."
#~ msgstr ""
#~ "如果本扩展程序没能自动检测到您 GNOME Shell 的主题的日间样式和夜间样式的"
#~ "话，您可以手动设置样式。想让扩展能成功识别您的主题？您可以<a "
#~ "href=\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-"
#~ "extension/-/issues\">提交请求</a>。"
